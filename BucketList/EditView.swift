//
//  EditView.swift
//  BucketList
//
//  Created by Hariharan S on 21/06/24.
//

import SwiftUI

struct EditView: View {
    enum LoadingState {
        case loading, loaded, failed
    }
    
    var location: Location
    var onSave: (Location) -> Void
    
    @Environment(\.dismiss) var dismiss

    @State private var name: String
    @State private var description: String
    
    @State private var pages = [Page]()
    @State private var loadingState = LoadingState.loading
    
    init(
        location: Location,
        onSave: @escaping (Location) -> Void
    ) {
        self.location = location
        self.onSave = onSave

        _name = State(initialValue: location.name)
        _description = State(initialValue: location.description)
    }

    var body: some View {
        NavigationStack {
            Form {
                Section {
                    TextField(
                        "Place name",
                        text: self.$name
                    )
                    TextField(
                        "Description",
                        text: self.$description
                    )
                }
                
                Section("Nearby…") {
                    switch self.loadingState {
                    case .loaded:
                        ForEach(
                            self.pages,
                            id: \.pageid
                        ) { page in
                            Text(page.title)
                                .font(.headline)
                            + Text(": ") +
                            Text(page.description)
                                .italic()
                        }
                    case .loading:
                        Text("Loading…")
                    case .failed:
                        Text("Please try again later.")
                    }
                }

            }
            .navigationTitle("Place details")
            .toolbar {
                Button("Save") {
                    var newLocation = self.location
                    newLocation.name = self.name
                    newLocation.description = self.description
                    self.onSave(newLocation)
                    self.dismiss()
                }
            }
            .task {
                await self.fetchNearbyPlaces()
            }
        }
    }
    
    func fetchNearbyPlaces() async {
        let urlString = "https://en.wikipedia.org/w/api.php?ggscoord=\(self.location.latitude)%7C\(self.location.longitude)&action=query&prop=coordinates%7Cpageimages%7Cpageterms&colimit=50&piprop=thumbnail&pithumbsize=500&pilimit=50&wbptterms=description&generator=geosearch&ggsradius=10000&ggslimit=50&format=json"

        guard let url = URL(string: urlString) 
        else {
            print("Bad URL: \(urlString)")
            return
        }

        do {
            let (data, _) = try await URLSession.shared.data(from: url)
            let items = try JSONDecoder().decode(Result.self, from: data)

            self.pages = items.query.pages.values.sorted()
            self.loadingState = .loaded
        } catch {
            // if we're still here it means the request failed somehow
            self.loadingState = .failed
        }
    }
}

#Preview {
    EditView(location: .example) { _ in }
}
