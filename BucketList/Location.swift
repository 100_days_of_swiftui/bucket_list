//
//  Location.swift
//  BucketList
//
//  Created by Hariharan S on 15/06/24.
//

import Foundation
import MapKit

struct Location: Codable, Equatable, Identifiable {
    let id: UUID
    var name: String
    var latitude: Double
    var longitude: Double
    var description: String
    
    static let example = Location(
        id: UUID(),
        name: "Buckingham Palace",
        latitude: 51.501,
        longitude: -0.141,
        description: "Lit by over 40,000 lightbulbs."
    )
    
    var coordinate: CLLocationCoordinate2D {
        CLLocationCoordinate2D(
            latitude: self.latitude,
            longitude: self.longitude
        )
    }
}
