//
//  BucketListApp.swift
//  BucketList
//
//  Created by Hariharan S on 15/06/24.
//

import SwiftUI

@main
struct BucketListApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
