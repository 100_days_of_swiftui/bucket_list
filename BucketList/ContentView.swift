//
//  ContentView.swift
//  BucketList
//
//  Created by Hariharan S on 15/06/24.
//

import MapKit
import SwiftUI

struct ContentView: View {
    
    @State private var viewModel = ViewModel()
    
    let startPosition = MapCameraPosition.region(
        MKCoordinateRegion(
            center: CLLocationCoordinate2D(
                latitude: 13.0843,
                longitude: 80.2705
            ),
            span: MKCoordinateSpan(
                latitudeDelta: 10,
                longitudeDelta: 10
            )
        )
    )
    
    var body: some View {
        NavigationStack {
            MapReader { proxy in
                Map(initialPosition: self.startPosition) {
                    ForEach(self.viewModel.locations) { location in
                        Annotation(
                            location.name,
                            coordinate: location.coordinate
                        ) {
                            Image(systemName: "star.circle")
                                .resizable()
                                .foregroundStyle(.red)
                                .frame(width: 20, height: 20)
                                .background(.white)
                                .clipShape(.circle)
                                .onLongPressGesture {
                                    self.viewModel.selectedPlace = location
                                }
                        }
                    }
                }
                .mapStyle(self.viewModel.mapStyle == .standard ? .standard : .hybrid)
                .onTapGesture { position in
                    if let coordinate = proxy.convert(
                        position,
                        from: .local
                    ) {
                        self.viewModel.addLocation(at: coordinate)
                    }
                }
                .sheet(item: self.$viewModel.selectedPlace) { place in
                    EditView(location: place) {
                        self.viewModel.update(location: $0)
                    }
                }
            }
            .navigationTitle("Bucket List")
            .navigationBarTitleDisplayMode(.inline)
            .toolbar{
                ToolbarItem(placement: .topBarTrailing) {
                    Button(
                        action: {
                            self.viewModel.toggleMapStyle()
                        },
                        label: {
                            Image(systemName: "map.fill")
                                .resizable()
                                .frame(width: 20, height: 20)
                        }
                    )
                }
            }
        }
    }
}


extension ContentView {
    @Observable
    class ViewModel {
        var isUnlocked = false
        var selectedPlace: Location?
        var mapStyle: CustomMapStyle = .standard
        private(set) var locations = [Location]()
        
        let savePath = URL.documentsDirectory.appending(path: "SavedPlaces")
        
        init() {
            do {
                let data = try Data(contentsOf: savePath)
                self.locations = try JSONDecoder().decode([Location].self, from: data)
            } catch {
                self.locations = []
            }
        }
        
        func addLocation(at point: CLLocationCoordinate2D) {
            let newLocation = Location(
                id: UUID(),
                name: "New location",
                latitude: point.latitude,
                longitude: point.longitude,
                description: ""
            )
            locations.append(newLocation)
            self.save()
        }
        
        func save() {
            do {
                let data = try JSONEncoder().encode(locations)
                try data.write(to: self.savePath, options: [.atomic, .completeFileProtection])
            } catch {
                print("Unable to save data.")
            }
        }
        
        func toggleMapStyle() {
            self.mapStyle = self.mapStyle == .standard ? .hybrid : .standard
        }
        
        func update(location: Location) {
            guard let selectedPlace 
            else { 
                return
            }

            if let index = locations.firstIndex(of: selectedPlace) {
                locations[index] = location
                self.save()
            }
        }
    }
}

#Preview {
    ContentView()
}


enum CustomMapStyle {
    case hybrid
    case standard
}
